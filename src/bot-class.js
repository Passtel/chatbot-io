const Bot = class Bot {
  constructor(name, avatar, color, actions, isMyself) {
    this.name = name;
    this.avatar = avatar;
    this.color = color;
    this.actions = actions;
    this.isMyself = isMyself;
  }
};

export default Bot;
