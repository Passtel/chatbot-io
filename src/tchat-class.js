import Message from './message-class';

const Tchat = class Tchat {
  renderHeader() {
    return `
        <header>
          <nav class="navbar navbar-dark bg-dark">
            <div class="container-fluid">
              <span class="navbar-brand mb-0 h1">
              <span class="text-warning">
                <strong>Salon de coiffure</strong>
              </span> / 
              <span class="text-light">Chatbot</span>
              </span>
            </div>
           </div>
          </nav>
        </header>
      `;
  }

  render() {
    return `
        <div id="app">
          ${this.renderHeader()}
          ${this.renderPageChat()}
        </div>
      `;
  }

  renderBotLeft(bot) {
    return `
        <ul class="list-group list-group-flush border border-primary rounded mb-2">
            <li class="bg-dark text-light list-group-item d-flex justify-content-between align-items-center">
                <img width="50" height="50" class="rounded-circle border border-white border-2" src="${bot.avatar}" /> <span style="color:${bot.color}">${bot.name}</span>
            </li>
        </ul>
      `;
  }

  renderBotRightOthers(msg, bot) {
    return `
        <div class="row mt-2">
            <div class="col-6">
                <div class="card text-bg-light">
                    <h5 class="card-header">
                        <img width="20%" height="100%" style="background-size: cover;" src="${bot.avatar}" class="rounded-circle img-thumbnail" alt="..."> <span style="color:${bot.color}">${bot.name}</span>
                    </h5>
                    <div class="card-body">
                        <h5 class="card-title">${msg.date}</h5>
                        <p class="card-text">${msg.message}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6"></div>
      `;
  }

  renderBotRightMyself(msg, bot) {
    return `
        <div class="row mt-2">
                    <div class="col-6"></div>
                    <div class="col-6">
                        <div class="card text-bg-light">
                            <h5 class="card-header">
                                <img width="20%" height="100%" style="background-size: cover;" src="${bot.avatar}" class="rounded-circle img-thumbnail" alt="..."><span style="color:${bot.color}">${bot.name}</span>
                            </h5>
                            <div class="card-body">
                                <h5 class="card-title">${msg.date}</h5>
                                <p class="card-text">${msg.message}</p>
                            </div>
                        </div>
                    </div>
                </div>
      `;
  }

  renderPageChat() {
    let html = `
      <div class="container-fluid">
        <div class="row">
            <div class="col-3">
              <div class="leftBox">`;
    JSON.parse(localStorage.getItem('botList')).forEach((bot) => {
      html += this.renderBotLeft(bot);
    });
    html += `</div></div><div class="col-9">
            <section class="messages-history">`;
    if (localStorage.getItem('msgList') !== '') {
      JSON.parse(localStorage.getItem('msgList')).forEach((msg) => {
        const botParsed = JSON.parse(msg.bot);
        if (botParsed.isMyself) {
          html += this.renderBotRightMyself(msg, botParsed);
        } else {
          html += this.renderBotRightOthers(msg, botParsed);
        }
      });
    }

    html += `</section>
            <section class="typing mt-3">
                <div class="row bottom-input">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="inputId" placeholder="Message">
                            <button class="btn btn-primary" id="clickMsg" type="button">Send</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
      </div>
    </div>
      `;
    return html;
  }

  sendMsg(inputValue) {
    JSON.parse(localStorage.getItem('botList')).forEach((bot) => {
      bot.actions.forEach((action) => {
        if (action[0] === inputValue) {
          this.writeMsgBot(action[1], bot, action[2]);
        }
      });
      if (bot.isMyself) {
        this.writeMsgBot(inputValue, bot, false);
      }
    });
  }

  writeMsgBot(mToType, bot, isFetch) {
    const msgList = localStorage.getItem('msgList');
    const msgListParsed = (msgList.length !== 0) ? JSON.parse(msgList) : [];
    const botStr = JSON.stringify(bot);
    if (isFetch) {
      this.fetchApi(mToType, msgListParsed, botStr);
    } else {
      this.saveMsg(mToType, msgListParsed, botStr);
    }
  }

  saveMsg(msgToSave, msgListParsed, botStr) {
    const msg = new Message(botStr, new Date().toLocaleDateString('fr-FR'), msgToSave, false);
    msgListParsed.push(msg);
    localStorage.setItem('msgList', JSON.stringify(msgListParsed));
    this.run();
  }

  fetchApi(url, msgListParsed, botStr) {
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        this.saveMsg(data[Object.keys(data)[0]], msgListParsed, botStr);
      });
  }

  run() {
    document.body.innerHTML = this.render();
    const inputMsg = document.getElementById('inputId');
    const btnSend = document.getElementById('clickMsg');
    btnSend.addEventListener('click', () => {
      if (inputMsg.value !== '') {
        this.sendMsg(inputMsg.value);
        inputMsg.value = '';
        window.scrollTo(0, document.body.scrollHeight);
      }
    });
    inputMsg.addEventListener('keyup', (e) => {
      if (e.key === 'Enter') {
        if (inputMsg.value !== '') {
          this.sendMsg(inputMsg.value);
          inputMsg.value = '';
          window.scrollTo(0, document.body.scrollHeight);
        }
      }
    });
  }
};

export default Tchat;
