import './index.scss';

import Bot from './bot-class';
import Tchat from './tchat-class';

const tchat = new Tchat();

// Initialisation des Objects
const bot1 = new Bot('Marie-Pierre', 'https://cdn.pixabay.com/photo/2021/01/04/10/37/icon-5887113_1280.png', '#E60EC5', [
  ['Bonjour', 'Salut, tu vas bien?', false],
  ['Au revoir', 'https://catfact.ninja/fact', true],
  ['Lets go', 'https://www.boredapi.com/api/activity', true]
], false);
const bot2 = new Bot('Sylvie', 'https://cdn-icons-png.flaticon.com/512/146/146025.png', '#CF6E07', [
  ['Bonjour', 'Yo !', false],
  ['Vive la france', 'https://official-joke-api.appspot.com/random_joke', true],
  ['Nombre de luc', 'https://api.genderize.io?name=luc', true]
], false);
const bot3 = new Bot('Christianne', 'https://cdn.pixabay.com/photo/2021/01/04/10/41/icon-5887126_960_720.png', '#12EFC7', [
  ['Bonjour', 'Wazaaaaa', false],
  ['Cool j\'ai plein d\'argent', 'https://www.boredapi.com/api/activity', true],
  ['Telephone', 'https://catfact.ninja/fact', true]
], false);

const myself = new Bot('Moi', 'https://cdn.pixabay.com/photo/2021/02/12/07/03/icon-6007530_960_720.png', '#FF0000', [], true);

const botList = [myself, bot1, bot2, bot3];

localStorage.setItem('isAlreadyLoaded', true);
localStorage.setItem('msgList', []);
localStorage.setItem('botList', JSON.stringify((botList)));

tchat.run();
