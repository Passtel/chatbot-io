const Message = class Message {
  constructor(bot, date, message) {
    this.bot = bot;
    this.date = date;
    this.message = message;
  }
};

export default Message;
